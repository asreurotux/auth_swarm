"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.profiles",
	table: "profile",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "profiles")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"name",
			"active",
			"created_at",
			"updated_at",
		],
		withRelateds: {},
		entityValidator: {
			name: { type: "string" },
			active: { type: "boolean" },
			$$strict: "remove"
		}
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
