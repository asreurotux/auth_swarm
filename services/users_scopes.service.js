"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.users_scopes",
	table: "user_scope",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "users_scopes")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"scope_id",
			"user_id",
		],
		withRelateds: {}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		scopes_of_user: {
			cache: {
				keys: ["user_id"],
				ttl: 60
			},
			rest: {
				path: "GET /users/:user_id/scopes"
			},
			params: {
				user_id: {
					type: "number", convert: true
				}
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						user_id: ctx.params.user_id
					}
				}).then(res => {
					return ctx.call("authorization.scopes.get", {
						id: res.map(r => r.scope_id)
					});
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
