"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.targets_conditions",
	table: "target_condition",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "targets_conditions")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"target_id",
			"field_name",
			"field_op",
			"field_value",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [],
		withRelateds: {},
		entityValidator: {
			target_id: { type: "number", convert: true },
			field_name: { type: "string" },
			field_op: {
				type: "enum", values: [
					"<",
					"<=",
					">",
					">=",
					"=",
					"!=",
					"IN",
					"NOTIN",
				]
			},
			field_value: { type: "string", convert: true },
		}
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_targets: {
			params: {
				target_id: { type: "string", convert: true },
				conditions: {
					type: "array",
					item: {
						field_name: { type: "string" },
						field_op: {
							type: "enum", values: [
								"<",
								"<=",
								">",
								">=",
								"=",
								"!=",
								"IN",
								"NOTIN",
							]
						},
						field_value: { type: "string", convert: true },
					},
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					target_id: ctx.params.target_id
				});
				this.clearCache();

				const entities = ctx.params.conditions.map(condition => ({
					target_id: ctx.params.target_id,
					field_name: condition.field_name,
					field_op: condition.field_op,
					field_value: condition.field_value,
					created_at: new Date(),
					updated_at: new Date()
				}));
				return this._insert(ctx, { entities });
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
