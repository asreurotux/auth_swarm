"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { EntityNotFoundError } = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.scopes",
	table: "scope",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "scopes")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"name",
			"permission",
			"create",
			"read",
			"update",
			"delete",
			"approve",
			"reject",
			"global"
		],
		defaultWithRelateds: [
			"user_groups"
		],
		withRelateds: {
			"user_groups"(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map(doc => {
						ctx.call("authorization.user_groups_scopes.user_groups_of_scope",
							{ scope_id: doc.id }
						).then(res => doc.user_groups = res);
					})
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			permission: { type: "string" },
			create: { type: "boolean" },
			read: { type: "boolean" },
			update: { type: "boolean" },
			delete: { type: "boolean" },
			approve: { type: "boolean" },
			reject: { type: "boolean" },
			global: { type: "boolean" },
			user_group_ids: {
				type: "array", items: {
					type: "number",
					positive: true,
					integer: true
				},
			},
			$$strict: "remove"
		}
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateUserGroups"
			],
			update: [
				"validateUserGroups"
			],
			patch: [
				"validateUserGroups"
			]
		},
		after: {
			create: [
				"saveUserGroups"
			],
			update: [
				"saveUserGroups"
			],
			patch: [
				"saveUserGroups"
			]
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		findAllUserAvailableScopesStrings: {
			cache: {
				keys: ["user_id"],
				ttl: 60
			},
			params: {
				user_id: { type: "number", convert: true }
			},
			async handler(ctx) {
				const userScopes = (await ctx.call("authorization.users_scopes.scopes_of_user", { user_id: ctx.params.user_id }));
				const userGroups = (await ctx.call("authorization.users_user_groups.user_groups_of_user", { user_id: ctx.params.user_id }));
				const publicScopes = (await ctx.call("authorization.scopes.find", { query: {
					global: 1
				} }));

				let allScopes = await Promise.all(userGroups.map((gr) => {
					return ctx.call("authorization.user_groups_scopes.scopes_of_groups", {
						user_group_id: gr.id
					});
				}));
				allScopes = allScopes.flat();

				allScopes = [
					...userScopes,
					...allScopes,
					...publicScopes
				];

				const scopeItems = [];

				allScopes.map((sc) => {
					if (this.hasFullAccess(sc)) {
						scopeItems.push(sc.permission);
					} else {
						scopeItems.push(this.getScopeString(sc, "create"));
						scopeItems.push(this.getScopeString(sc, "read"));
						scopeItems.push(this.getScopeString(sc, "update"));
						scopeItems.push(this.getScopeString(sc, "delete"));
						scopeItems.push(this.getScopeString(sc, "approve"));
						scopeItems.push(this.getScopeString(sc, "reject"));
					}

					return sc;
				});
				return scopeItems.filter(el => el && el !== "");
			}
		}


	},

	/**
	 * Events
	 */
	events: {
	},

	/**
	 * Methods
	 */
	methods: {
		hasFullAccess(sc) {
			return sc.create && sc.read && sc.update && sc.delete && sc.approve && sc.reject;
		},
		getScopeString(scope, item) {
			return scope[item] && scope[item] ? `${scope.permission}:${item}` : "";
		},
		async saveUserGroups(ctx, response) {
			if (ctx.params.user_group_ids && Array.isArray(ctx.params.user_group_ids)) {
				const user_group_ids = [...new Set(ctx.params.user_group_ids)];
				await ctx.call("authorization.user_groups_scopes.save_groups_of_scopes", {
					scope_id: response[0].id,
					user_group_ids
				});
				response[0].user_groups = await ctx.call("authorization.user_groups_scopes.user_groups_of_scope", {
					scope_id: response[0].id
				});
			}
			return response;
		},
		async validateUserGroups(ctx) {
			if (ctx.params.user_group_ids && Array.isArray(ctx.params.user_group_ids)) {
				const user_group_ids = [...new Set(ctx.params.user_group_ids)];

				const scopes = await ctx.call("authorization.user-groups.count", {
					query: {
						id: user_group_ids
					}
				});

				if (scopes < user_group_ids.length) {
					throw new EntityNotFoundError("scopes", null);
				}

			} else {
				ctx.scope_ids = [];
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
