"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;
const { EntityNotFoundError } = require("@fisas/ms_core").Helpers.Errors;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.user-groups",
	table: "user_group",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "user-groups")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"name"
		],
		defaultWithRelateds: ["scopes"],
		withRelateds: {
			"scopes"(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map(doc => {
						ctx.call("authorization.user_groups_scopes.scopes_of_groups",
							{ user_group_id: doc.id }
						).then(res => doc.scopes = res);
					})
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			scope_ids: {
				type: "array", items: {
					type: "number",
					positive: true,
					integer: true
				},
			},
			$$strict: "remove"
		}
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				"validateScopes"
			],
			update: [
				"validateScopes"
			],
			patch: [
				"validateScopes"
			]
		},
		after: {
			create: [
				"saveScopes"
			],
			update: [
				"saveScopes"
			],
			patch: [
				"saveScopes"
			]
		}
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		async saveScopes(ctx, response) {
			if (ctx.params.scope_ids && Array.isArray(ctx.params.scope_ids)) {
				const scopes_ids = [...new Set(ctx.params.scope_ids)];
				await ctx.call("authorization.user_groups_scopes.save_scopes_of_group", {
					user_group_id: response[0].id,
					scopes_ids
				});
				response[0].scopes = await ctx.call("authorization.user_groups_scopes.scopes_of_groups", {
					user_group_id: response[0].id
				});
			}
			return response;
		},
		async validateScopes(ctx) {
			if (ctx.params.scope_ids && Array.isArray(ctx.params.scope_ids)) {
				const scopes_ids = [...new Set(ctx.params.scope_ids)];

				const scopes = await ctx.call("authorization.scopes.count", {
					query: {
						id: scopes_ids
					}
				});

				if (scopes < scopes_ids.length) {
					throw new EntityNotFoundError("scopes", null);
				}

			} else {
				ctx.scope_ids = [];
			}
		}
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
