"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.targets",
	table: "target",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "targets")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"name",
			"active",
			"created_at",
			"updated_at",
		],
		defaultWithRelateds: [
			"conditions"
		],
		withRelateds: {
			"conditions"(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map(doc => {
						ctx.call("authorization.targets_conditions.find", {
							query: {
								target_id: doc.id
							}
						}).then(res => doc.conditions = res);
					})
				);
			},
		},
		entityValidator: {
			name: { type: "string" },
			active: { type: "boolean" },
			conditions: {
				type: "array",
				item: {
					field_name: { type: "string" },
					field_op: { type: "enum", values: [
						"<",
						"<=",
						">",
						">=",
						"=",
						"!=",
						"IN",
						"NOTIN",
					] },
					field_value: { type: "string", convert: true },
				},
				optional: true
			}
		}
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				function sanatizeParams(ctx) {
					ctx.params.created_at = new Date();
					ctx.params.updated_at = new Date();
				}
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		},
		after: {
			create: [
				"saveTargetConditions"
			],
			update: [
				"saveTargetConditions"
			],
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		async saveTargetConditions(ctx, res) {
			if (ctx.params.conditions && Array.isArray(ctx.params.conditions)) {
				await ctx.call("authorization.targets_conditions.save_targets", {
					target_id: res[0].id,
					conditions: ctx.params.conditions
				});
				res[0].conditions = await ctx.call("authorization.targets_conditions.find", {
					query: {
						target_id: res[0].id
					}
				});
			}
			return res;
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
