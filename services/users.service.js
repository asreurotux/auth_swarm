"use strict";
const crypto = require("crypto");
const uuid = require("uuid").v4;
const { MoleculerError } = require("moleculer").Errors;
const { Errors } = require("@fisas/ms_core").Helpers;
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

const _ = require("lodash");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.users",
	table: "user",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "users")],

	/**
	 * Settings
	 */
	settings: {
		fields: [
			"id",
			"rfid",
			"rfid_validity",
			"name",
			"email",
			"phone",
			"user_name",
			"first_login",
			"institute",
			"student_number",
			"can_change_password",
			"identification",
			"external",
			"can_access_BO",
			"active",
			"birth_date",
			"gender",
			"picture",
			"updated_at",
			"created_at",
			"fb_messenger_id",
			"address",
			"postal_code",
			"city",
			"country",
			"tin",
			"profile_id",
		],
		defaultWithRelateds: [
			"profile",
			"scopes",
			"user_groups",
		],
		withRelateds: {
			"profile"(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map(doc => {
						ctx.call("authorization.profiles.get",
							{
								id: doc.profile_id
							}
						).then(res => doc.profile = (res.length > 0 ? res[0] : null));
					})
				);
			},
			"scopes"(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map(doc => {
						ctx.call("authorization.users_scopes.scopes_of_user",
							{ user_id: doc.id }
						).then(res => doc.scopes = res);
					})
				);
			},
			"user_groups"(ids, docs, rule, ctx) {
				return Promise.all(
					docs.map(doc => {
						ctx.call("authorization.users_user_groups.user_groups_of_user",
							{ user_id: doc.id }
						).then(res => doc.user_groups = res);
					})
				);
			},
		},
		entityValidator: {
			rfid: { type: "string", optional: true },
			rfid_validity: { type: "string", optional: true },
			name: { type: "string" },
			email: { type: "email" },
			phone: { type: "string" },
			user_name: { type: "string" },
			institute: { type: "string" },
			student_number: { type: "string" },
			identification: { type: "string" },
			external: { type: "boolean" },
			pin: { type: "string", length: 4 },
			password: { type: "string", min: 5 },
			can_access_BO: { type: "boolean" },
			active: { type: "boolean" },
			birth_date: { type: "date", convert: true },
			gender: { type: "enum", values: ["M", "F", "U"] },
			picture: { type: "string" },
			fb_messenger_id: { type: "string" },
			address: { type: "string" },
			postal_code: { type: "string" },
			city: { type: "string" },
			country: { type: "string" },
			tin: { type: "string" },
			profile_id: { type: "number", positive: true, integer: true, convert: true },
			scope_ids: {
				type: "array", items: {
					type: "number",
					positive: true,
					integer: true
				},
				optional: true
			},
			user_group_ids: {
				type: "array", items: {
					type: "number",
					positive: true,
					integer: true
				},
				optional: true
			},
			$$strict: "remove"
		},
	},

	/**
	 * Hooks
	 */
	hooks: {
		before: {
			create: [
				async function checkRFID(ctx) {
					if (ctx.params.rfid) {
						const rfids = await this._find(ctx, {
							query: {
								rfid: ctx.params.rfid
							}
						});

						if (rfids.length > 0) {
							throw new Errors.ValidationError([
								{
									field: "rfid",
									type: "exists",
									message: "Already exist a user with that rfid"
								}
							]);
						}
					}
				},
				async function checkEmail(ctx) {
					const userNames = await this._find(ctx, {
						query: {
							email: ctx.params.email
						}
					});

					if (userNames.length > 0) {
						throw new Errors.ValidationError([
							{
								field: "email",
								type: "exists",
								message: "Already exist a user with that email"
							}
						]);
					}
				},
				async function checkUsername(ctx) {
					const userNames = await this._find(ctx, {
						query: {
							user_name: ctx.params.user_name
						}
					});

					if (userNames.length > 0) {
						throw new Errors.ValidationError([
							{
								field: "user_name",
								type: "exists",
								message: "Already exist a user with that user_name"
							}
						]);
					}
				},
				async function checkRelations(ctx) {

					// CHECK IF PROFILE EXIST
					const profile = await ctx.call("authorization.profiles.get", { id: ctx.params.profile_id });
					if (profile.length === 0) {
						throw new Errors.EntityNotFoundError("profiles", ctx.params.profile_id);
					}

					// CHECK IF SCOPES EXIST
					if (ctx.params.scope_ids && Array.isArray(ctx.params.scope_ids)) {
						ctx.params.scope_ids = _.uniq(ctx.params.scope_ids);
						const scopes = await Promise.all(ctx.params.scope_ids.map(scope_id => ctx.call("authorization.scopes.get", { id: scope_id })));
						scopes.map(s => {
							if (s.length === 0) {
								throw new Errors.EntityNotFoundError("scopes", ctx.params.scope_ids);
							}
						});

					}

					// CHECK IF USER_GROUPS EXIST
					if (ctx.params.user_group_ids && Array.isArray(ctx.params.user_group_ids)) {
						ctx.params.user_group_ids = _.uniq(ctx.params.user_group_ids);
						const user_groups = await Promise.all(ctx.params.user_group_ids.map(user_group_id => ctx.call("authorization.scopes.get", { id: user_group_id })));
						user_groups.map(s => {
							if (s.length === 0) {
								throw new Errors.EntityNotFoundError("user_groups", ctx.params.user_group_ids);
							}
						});

					}
				},
			],
			update: [
				function sanatizeParams(ctx) {
					ctx.params.updated_at = new Date();
				}
			]
		},
		after: {
			create: [
			]
		}
	},

	/**
	 * Dependencies
	 */
	dependencies: [
	],

	/**
	 * Actions
	 */
	actions: {
		checkPassword: {
			params: {
				password: { type: "string" },
				user_id: { type: "number" }
			},
			async handler(ctx) {
				return this.findPasswordOfUser(ctx.params.user_id).then(user => {
					if (!user) {
						throw new MoleculerError("User not found", 404);
					}

					const hashBody = this.hashPassword(ctx.params.password, user[0].salt);
					return hashBody === user[0].password;
				});
			}
		},
		checkPin: {
			params: {
				pin: { type: "string", length: 4 },
				user_id: { type: "number" }
			},
			async handler(ctx) {
				return this.getById(ctx.params.user_id).then(user => {
					if (!user) {
						throw new MoleculerError("User not found", 404);
					}

					const hashBody = this.hashPin(ctx.params.pin, user.salt);

					return hashBody === user.pin;
				});
			}
		},
		update_me: {
			rest: {
				method: "PUT",
				path: "/update-me"
			},
			async handler() {
				return "Hello Moleculer";
			}
		},
		create: {
			rest: "POST /",
			scope: "authorization:users:create",
			handler(ctx) {
				let entity = ctx.params;
				const { scope_ids, user_group_ids } = entity;
				delete entity.scope_ids;
				delete entity.user_group_ids;

				return this.validateEntity(entity)
					// Hash password & pin and initialize fields
					.then(entity => {
						entity.first_login = false;
						entity.can_change_password = 0;
						entity.updated_at = new Date();
						entity.created_at = new Date();
						entity.salt = this.generateSalt();
						entity.password = this.hashPassword(entity.password, entity.salt);
						entity.pin = this.hashPin(entity.pin, entity.salt);
						return entity;
					})
					// Apply idField
					.then(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
					.then(entity => this.adapter.insert(entity))
					// Create scopes relation
					.then(async entity => {
						ctx.call("authorization.users_scopes.insert",
							scope_ids.map(s => ({ user_id: entity.id, scope_id: s }))
						);
						return entity;
					})
					// Create user_groups relation
					.then(entity => {
						ctx.call("authorization.users_user_groups.insert",
							user_group_ids.map(ug => ({ user_id: entity.id, user_group_id: ug }))
						);
						return entity;
					})
					.then(entity => this.adapter.findById(entity[0]))
					.then(doc => this.transformDocuments(ctx, {}, doc))
					.then(json => this.entityChanged("created", json, ctx).then(() => json));
			}
		},
		/*patch: {
			rest: "PATCH /",
			scope: "authorization:users:update",
			handler(ctx) {
				let entity = ctx.params;
				const {scope_ids, user_group_ids} = entity;
				delete entity.scope_ids;
				delete entity.user_group_ids;

				return this.validateEntity(entity)
					// Apply idField
					.then(entity => this.adapter.beforeSaveTransformID(entity, this.settings.idField))
					.then(entity => this.adapter.update(entity))
					// Create scopes relation
					.then(async entity => {
						ctx.call("authorization.users_scopes.insert",
							scope_ids.map(s => ({ user_id: entity.id, scope_id: s }))
						);
						return entity;
					})
					// Create user_groups relation
					.then(entity => {
						ctx.call("authorization.users_user_groups.insert",
							user_group_ids.map(ug => ({ user_id: entity.id, user_group_id: ug }))
						);
						return entity;
					})
					.then(entity => this.adapter.findById(entity[0]))
					.then(doc => this.transformDocuments(ctx, {}, doc))
					.then(json => this.entityChanged("created", json, ctx).then(() => json));
			}
		},*/
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		findPasswordOfUser(id) {
			return this.adapter.findById(id);
		},
		generateSalt() {
			return uuid();
		},
		hashPassword(password, salt) {
			return crypto.createHash("sha256").update(salt + password).digest("hex");
		},
		hashPin(pin, salt) {
			return crypto.createHash("sha256").update(salt + pin).digest("hex");
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
