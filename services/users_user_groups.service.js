"use strict";
const {  DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.users_user_groups",
	table: "user_user_group",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "users_user_groups")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"user_id",
			"user_group_id",
		],
		withRelateds: {}
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		user_groups_of_user: {
			cache: {
				keys: ["user_id"],
				ttl: 60
			},
			rest: {
				path: "GET /users/:user_id/user-groups"
			},
			params: {
				user_id: {
					type: "number", convert: true
				}
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						user_id: ctx.params.user_id
					}
				}).then(res => {
					return ctx.call("authorization.user-groups.get", {
						id: res.map(r => r.user_group_id)
					});
				});
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
