"use strict";
const { MoleculerError } = require("moleculer").Errors;
const { Errors } = require("@fisas/ms_core").Helpers;
const jwt = require("jsonwebtoken");
const TokenExpiredError = jwt.TokenExpiredError;

/**
 * @typedef {import("moleculer").Context} Context Moleculer"s Context
 */

module.exports = {
	name: "authorization.authorize",

	/**
	 * Settings
	 */
	settings: {

	},

	mixins: [],
	/**
	 * Dependencies
	 */
	dependencies: ["configuration.devices"],

	/**
	 * Actions
	 */
	actions: {

		login_by_device: {
			authorization: false,
			authentication: false,
			rest: {
				method: "POST",
				path: "/device/:deviceId"
			},
			params: {
				deviceId: { type: "number", convert: true },
				email: { type: "email", optional: true },
				user_name: { type: "string", optional: true },
				pin: { type: "string", optional: true },
				password: { type: "string", optional: true },
				rfid: { type: "string", optional: true }
			},
			async handler(ctx) {

				const device = await ctx.call("configuration.devices.get", { id: ctx.params.deviceId });

				if (device.active === false) {
					throw new Errors.ValidationError("Device not active", "DEVICE_NOT_ACTIVE", { device: "deviceId" });
				}

				return this.performeAuthFlow(ctx, ctx.params, device);
			}
		},
		login_by_device_type: {
			authorization: false,
			authentication: false,
			rest: {
				authorization: false,
				method: "POST",
				path: "/device-type/:type"
			},
			params: {
				type: { type: "string" },
				email: { type: "email", optional: true },
				user_name: { type: "string", optional: true },
				pin: { type: "string", optional: true },
				password: { type: "string", optional: true },
				rfid: { type: "string", optional: true }
			},
			async handler(ctx) {
				const supportedDeviceTypes = ["MOBILE", "BO", "WEB", "POS"];

				if (!supportedDeviceTypes.includes(ctx.params.type))
					throw new MoleculerError(`The device type provided cannot be authorized using this endpoint (only available for [${supportedDeviceTypes}]). Please use the endpoint /api/v1/authorize/device/{id} (providing the ID of the device you are trying to authenticate).`, 400);

				const devices = await ctx.call("configuration.devices.find", { query: { type: ctx.params.type } });

				if (!devices || devices.length === 0) {
					throw new Errors.EntityNotFoundError("devices", ctx.params.type);
				}

				if (devices[0].active === false) {
					throw new Errors.ValidationError("Device not active", "DEVICE_NOT_ACTIVE", { device: "deviceId" });
				}

				return this.performeAuthFlow(ctx, ctx.params, devices[0]);
			}
		},
		validate_token: {
			authorization: false,
			authentication: false,
			rest: {
				method: "GET",
				path: "/validate-token/:token"
			},
			cache: {
				keys: ["token"],
				ttl: 60
			},
			scope: "authorization:validate-token:create",
			params: {
				token: { type: "string" }
			},
			async handler(ctx) {
				let tokenInfo = null;
				try {
					tokenInfo = jwt.verify(ctx.params.token, process.env.JWT_SECRET_KEY);
				} catch (err) {
					if (err instanceof TokenExpiredError) {
						throw new MoleculerError("Token expired", 401, "ERR_TOKEN_EXPIRED");
					}

					throw new MoleculerError("Token invalid", 401, "ERR_INVALID_TOKEN");
				}

				if (!tokenInfo.user || !tokenInfo.user.id) {
					return;
				}

				const user = await this.findActiveUser(ctx, "id", tokenInfo.user.id);
				const scopes = await ctx.call("authorization.scopes.findAllUserAvailableScopesStrings", {
					user_id: user.id
				});
				return { user, scopes };
			}
		},
		logged_user: {
			rest: {
				method: "GET",
				path: "/user"
			},
			async handler() {
				return "Hello Moleculer";
			}
		},
	},

	/**
	 * Events
	 */
	events: {
		"authorization.*"(ctx) {
			this.logger.error("BROADCAST EVENT 2");
		},
		"authorization.user_groups_scopes.created"(ctx) {
			this.logger.error("BROADCAST EVENT 1");
			this.clearCache();
		},
		"authorization.user_groups_scopes.updated"(ctx) {
			this.logger.error("BROADCAST EVENT 1");
			this.clearCache();
		},
		"authorization.user_groups_scopes.removed"(ctx) {
			this.logger.error("BROADCAST EVENT 1");
			this.clearCache();
		},
		"authorization.user_scopes.created"(ctx) {
			this.logger.error("BROADCAST EVENT 1");
			this.clearCache();
		},
		"authorization.users_scopes.updated"(ctx) {
			this.logger.error("BROADCAST EVENT 1");
			this.clearCache();
		},
		"authorization.users_scopes.removed"(ctx) {
			this.logger.error("BROADCAST EVENT 1");
			this.clearCache();
		},
		"authorization.users_user_groups.created"(ctx) {
			this.logger.error("BROADCAST EVENT 1");
			this.clearCache();
		},
		"authorization.users_user_groups.updated"(ctx) {
			this.logger.error("BROADCAST EVENT 1");
			this.clearCache();
		},
		"authorization.users_user_groups.removed"(ctx) {
			this.logger.error("BROADCAST EVENT 1");
			this.clearCache();
		},
	},

	/**
 	* Methods
 	*/
	methods: {
		findActiveUser(ctx, prop, value) {
			const params = {
				query: {
					active: 1
				}
			};
			params["query"][prop] = value;

			// CEHCK RFID_VALIDITY
			if (prop === "rfid") {
				//params["query"]["rfid_validity"] = value; TODO
			}

			return ctx.call("authorization.users.find", params).then(res => {
				if (Array.isArray(res) & res.length > 0) {
					return res[0];
				} else {
					throw new Errors.ValidationError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {});
				}
			});
		},
		async performeAuthFlow(ctx, params, device) {
			this.validateEmailUsername(params);
			this.validatePinPassword(params);

			let valid = false;
			let user = null;

			// Case 1: email + password combo
			if (params.email && params.password) {
				user = await this.findActiveUser(ctx, "email", params.email);

				valid = await ctx.call("authorization.users.checkPassword", {
					user_id: user.id,
					password: ctx.params.password
				});

				if (!valid) {
					throw new Errors.ValidationError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {});
				}
			}

			if (!valid) {
				// Case 2: email + pin combo
				if (params.email && params.pin) {
					user = await this.findActiveUser(ctx, "email", params.email);

					valid = await ctx.call("authorization.users.checkPin", {
						user_id: user.id,
						pin: ctx.params.pin
					});

					if (!valid) {
						throw new Errors.ValidationError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {});
					}
				}
			}

			if (!valid) {
				// Case 3: user_name + password combo
				if (params.user_name && params.password) {
					user = await this.findActiveUser(ctx, "user_name", params.user_name);

					valid = await ctx.call("authorization.users.checkPassword", {
						user_id: user.id,
						password: ctx.params.password
					});

					if (!valid) {
						throw new Errors.ValidationError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {});
					}
				}
			}

			if (!valid) {
				// Case 4: user_name + pin combo
				if (params.user_name && params.pin) {
					user = await this.findActiveUser(ctx, "user_name", params.user_name);

					valid = await ctx.call("authorization.users.checkPin", {
						user_id: user.id,
						pin: ctx.params.pin
					});

					if (!valid) {
						throw new Errors.ValidationError("Invalid credentials", "ERR_INVALID_CREDENTIALS", {});
					}
				}
			}

			if (!valid) {
				// Case 5: RFID
				if (params.rfid) {
					user = await this.findActiveUser(ctx, "rfid", params.rfid);
					valid = true;
				}
			}

			if (!valid) {
				// Case 6: simple token for device
				user = undefined;
				valid = true;
			}

			if (device.type === "BO") {
				// Addition authorization level: can user ccess BO?
				if (!user || !user.can_access_BO) {
					throw new MoleculerError("no permission to access backoffice", 401, "ERR_NO_ACCESS_BACKOFFICE");
				}
			}

			return this.generateTokenFromUser(ctx, {
				user,
				device,
			});
		},
		async generateTokenFromUser(ctx, {
			user,
			device,
		}) {
			// Set default data in the token payload
			let payload = {
				user: {},
				deviceId: device.id,
				device
			};
			let isGuest;

			// Check if we are handling a concrete user or a guest user
			if (user) {
				isGuest = false;
			} else {
				// Find anonymous user in the database
				// eslint-disable-next-line no-param-reassign
				user = await this.findActiveUser(ctx, "user_name", "guest");
				isGuest = true;
			}

			payload = Object.assign({}, payload, {
				user,
				isGuest
			});

			// Generate the token
			const token = jwt.sign(
				payload,
				process.env.JWT_SECRET_KEY, {
					expiresIn: "7 days",
				}, // Default expire date is one week
			);

			return { token };
		},
		validateEmailUsername(params) {
			if (
				// We have email or user_name
				(params.email || params.user_name)
				// We do not have password nor pin
				&&
				(!params.password && !params.pin)
			) {
				throw new MoleculerError("'Password' or 'pin' are required if an email/user_name is provided.", 400);
			}
		},
		validatePinPassword(params) {
			if (
				// We have password or pin
				(params.password || params.pin)
				// We do not have email or user_name
				&&
				(!params.email && !params.user_name)
			) {
				throw new MoleculerError("'Email' or 'user_name' are required if an pin/password is provided.", 400);
			}
		},
		clearCache() {
			this.broker.broadcast(`cache.clean.${this.fullName}`);
			if (this.broker.cacher)
				return this.broker.cacher.clean(`${this.fullName}.*`);
			return Promise.resolve();
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
