"use strict";
const { DbMixin, KnexAdpater } = require("@fisas/ms_core").Mixins;

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "authorization.user_groups_scopes",
	table: "user_group_scope",

	adapter: new KnexAdpater(require("../knexfile")),
	mixins: [DbMixin("authorization", "user_groups_scopes")],

	/**
	 * Settings
	*/
	settings: {
		fields: [
			"id",
			"scope_id",
			"user_group_id",
		],
		withRelateds: {
		}
	},

	/**
	 * Hooks
	 */
	hooks: {
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {
		save_scopes_of_group: {
			params: {
				user_group_id: {
					type: "number", convert: true
				},
				scopes_ids: {
					type: "array", items: {
						type: "number",
						positive: true,
						integer: true
					},
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					user_group_id: ctx.params.user_group_id
				});
				this.clearCache();

				const entities = ctx.params.scopes_ids.map(scope_id => ({ user_group_id: ctx.params.user_group_id, scope_id }));
				return this._insert(ctx, { entities });
			}
		},
		scopes_of_groups: {
			cache: {
				keys: ["user_group_id"],
				ttl: 60
			},
			rest: {
				path: "GET /user-groups/:user_group_id/scopes"
			},
			params: {
				user_group_id: {
					type: "number", convert: true
				}
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						user_group_id: ctx.params.user_group_id
					}
				}).then(res => {
					return ctx.call("authorization.scopes.find", {
						query: {
							id: res.map(usc => usc.scope_id)
						}
					});
				});
			}
		},
		user_groups_of_scope: {
			cache: {
				keys: ["scope_id"],
				ttl: 60
			},
			rest: {
				path: "GET /scopes/:scope_id/user-groups"
			},
			params: {
				scope_id: {
					type: "number", convert: true
				}
			},
			async handler(ctx) {
				return this._find(ctx, {
					query: {
						scope_id: ctx.params.scope_id
					}
				}).then(res => {
					return ctx.call("authorization.user-groups.find", {
						query: {
							id: res.map(usc => usc.user_group_id)
						}
					});
				});
			}
		},
		save_groups_of_scopes: {
			params: {
				scope_id: {
					type: "number", convert: true
				},
				user_group_ids: {
					type: "array", items: {
						type: "number",
						positive: true,
						integer: true
					},
				}
			},
			async handler(ctx) {
				await this.adapter.removeMany({
					scope_id: ctx.params.scope_id
				});
				this.clearCache();

				const entities = ctx.params.user_group_ids.map(user_group_id => ({ scope_id: ctx.params.scope_id, user_group_id }));
				return this._insert(ctx, { entities });
			}
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
