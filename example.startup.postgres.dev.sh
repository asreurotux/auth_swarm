# Creating database (schema migration)
docker exec -it fisas_authorization_services npm run knex:migration:migrate
# Populate with dummy data (seeds)
docker exec -it fisas_authorization_services npm run knex:seed:run
