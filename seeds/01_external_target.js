exports.seed = knex => knex('target').select()
  .where('name', 'External')
  .then(async (rows) => {
    if (rows.length === 0) {
      const p = await knex('target').insert({
        name: 'External',
        active: true,
        created_at: new Date(),
        updated_at: new Date(),
      });

      return knex('target_condition').insert({
        target_id: p.pop(),
        field_name: 'external',
        field_op: '=',
        field_value: 'true',
        created_at: new Date(),
        updated_at: new Date(),
      });
    }
    return true;
  })
  .catch((err) => {
    // eslint-disable-next-line no-console
    console.error(err);
  });
