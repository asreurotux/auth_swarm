const crypto = require('crypto');
const UUIDV4 = require('uuid').v4;

exports.seed = knex => knex('user').select()
  .where('user_name', 'guest')
  .then(async (rows) => {
    if (rows.length === 0) {
      const prof = await knex('profile').select().where('name', 'Alunos');

      await knex('user').insert({
        name: 'GUEST USER',
        email: 'guest@ipvc.pt',
        phone: '',
        user_name: 'guest',
        password: '',
        gender: 'F',
        pin: '',
        salt: '',
        external: 1,
        active: true,
        can_access_BO: 0,
        profile_id: prof[0].id,
        created_at: new Date(),
        updated_at: new Date(),
      });

      const salt = UUIDV4();
      await knex('user').insert({
        name: 'ADMIN USER',
        email: 'admin@ipvc.pt',
        phone: '',
        user_name: 'admin',
        password: crypto.createHash('sha256').update(salt.concat('admin')).digest('hex'),
        gender: 'M',
        salt,
        pin: crypto.createHash('sha256').update(salt.concat('1234')).digest('hex'),
        external: 1,
        active: true,
        profile_id: prof[0].id,
        can_access_BO: 1,
        created_at: new Date(),
        updated_at: new Date(),
      });
    }
    return true;
  })
  .catch((err) => {
    console.log(err);
  });
