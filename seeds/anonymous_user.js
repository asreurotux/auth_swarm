exports.seed = knex => knex('user').select()
  .where('user_name', 'guest')
  .then(async (rows) => {
    if (rows.length === 0) {
      await knex('user').insert({
        name: 'GUEST USER',
        email: 'guest@ipvc.pt',
        phone: '',
        user_name: 'guest',
        password: '',
        pin: '',
        salt: '',
        external: 1,
        active: true,
        created_at: new Date(),
        updated_at: new Date(),
      });
    }
    return true;
  })
  .catch((err) => {
    console.log(err);
  });
