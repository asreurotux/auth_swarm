# [1.2.0-rc.2](https://gitlab.com/asreurotux/auth_swarm/compare/v1.2.0-rc.1...v1.2.0-rc.2) (2021-02-03)


### Features

* **backmerge:** removed dependency ([3c56014](https://gitlab.com/asreurotux/auth_swarm/commit/3c56014c5cc33f45dbf435872a823ca7737906e8))

# [1.2.0-rc.1](https://gitlab.com/asreurotux/auth_swarm/compare/v1.1.0...v1.2.0-rc.1) (2021-02-03)


### Features

* **backmerge:** removed dependency ([c000d73](https://gitlab.com/asreurotux/auth_swarm/commit/c000d7315ed3b417e050b29939130a5ccec159aa))

# [1.1.0](https://gitlab.com/asreurotux/auth_swarm/compare/v1.0.0...v1.1.0) (2021-02-03)


### Features

* **release:** added semantic-release backmerge ([cbd8388](https://gitlab.com/asreurotux/auth_swarm/commit/cbd83880cd0656cba87462e3c44bf27bfffeb0d9))

# [1.0.0-rc.2](https://gitlab.com/asreurotux/auth_swarm/compare/v1.0.0-rc.1...v1.0.0-rc.2) (2021-02-03)


### Features

* **release:** added semantic-release backmerge ([cbd8388](https://gitlab.com/asreurotux/auth_swarm/commit/cbd83880cd0656cba87462e3c44bf27bfffeb0d9))

# 1.0.0-rc.1 (2020-10-28)


### Bug Fixes

* **ci:** added git plugin ([7df13d4](https://gitlab.com/asreurotux/auth_swarm/commit/7df13d4fe2c5a5c75c4d85792a04803aef2e6b9d))
* **semantic-release:** added branches ([0b3024b](https://gitlab.com/asreurotux/auth_swarm/commit/0b3024b105257e14bdd02c2909cbf231436e778b))

# 1.0.0-rc.1 (2020-10-28)


### Bug Fixes

* **ci:** added git plugin ([7df13d4](https://gitlab.com/asreurotux/auth_swarm/commit/7df13d4fe2c5a5c75c4d85792a04803aef2e6b9d))
* **semantic-release:** added branches ([0b3024b](https://gitlab.com/asreurotux/auth_swarm/commit/0b3024b105257e14bdd02c2909cbf231436e778b))

# 1.0.0-rc.1 (2020-10-27)


### Bug Fixes

* **ci:** added git plugin ([7df13d4](https://gitlab.com/asreurotux/auth_swarm/commit/7df13d4fe2c5a5c75c4d85792a04803aef2e6b9d))
* **semantic-release:** added branches ([0b3024b](https://gitlab.com/asreurotux/auth_swarm/commit/0b3024b105257e14bdd02c2909cbf231436e778b))

# 1.0.0-rc.1 (2020-10-27)


### Bug Fixes

* **ci:** added git plugin ([7df13d4](https://gitlab.com/asreurotux/auth_swarm/commit/7df13d4fe2c5a5c75c4d85792a04803aef2e6b9d))
* **semantic-release:** added branches ([0b3024b](https://gitlab.com/asreurotux/auth_swarm/commit/0b3024b105257e14bdd02c2909cbf231436e778b))

# 1.0.0-rc.1 (2020-10-27)


### Bug Fixes

* **ci:** added git plugin ([7df13d4](https://gitlab.com/asreurotux/auth_swarm/commit/7df13d4fe2c5a5c75c4d85792a04803aef2e6b9d))
* **semantic-release:** added branches ([0b3024b](https://gitlab.com/asreurotux/auth_swarm/commit/0b3024b105257e14bdd02c2909cbf231436e778b))

# 1.0.0-rc.1 (2020-10-26)


### Bug Fixes

* **ci:** added git plugin ([7df13d4](https://gitlab.com/asreurotux/auth_swarm/commit/7df13d4fe2c5a5c75c4d85792a04803aef2e6b9d))
* **semantic-release:** added branches ([0b3024b](https://gitlab.com/asreurotux/auth_swarm/commit/0b3024b105257e14bdd02c2909cbf231436e778b))
