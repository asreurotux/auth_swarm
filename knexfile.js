require("dotenv").config({ path: "./docker-compose.env" });

module.exports = {
	client: "postgresql",
	connection: {
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DATABASE,
  },
  migrations: {
    tableName: "migrations",
  },
  debug: process.env.DATABASE_DEBUG === "true",
};