module.exports.up = async db => db.schema.createTable('change_credentials', (table) => {
  table.increments();
  table.string('token', 255).notNullable();
  table.integer('user_id').unsigned().notNullable();
  table.enum('type', ['PIN', 'PASSWORD', 'RFID']);
  table.boolean('changed').default(false).notNullable();
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
});

module.exports.down = async db => db.schema
  .dropTable('change_credentials');

module.exports.configuration = { transaction: true };
