module.exports.up = async db => db.schema.alterTable('user', (table) => {
  table.string('identification', 15).after('student_number');
});

module.exports.down = async db => db.schema.alterTable('user', (table) => {
  table.dropColumn('identification');
});
