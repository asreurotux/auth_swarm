module.exports.up = async db => db.schema
  .table('scope', table => table.boolean('global').defaultTo(0));

module.exports.down = async db => db.schema
  .table('scope', table => table.dropColumn('global'));

module.exports.configuration = { transaction: true };
