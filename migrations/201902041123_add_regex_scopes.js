module.exports.up = async db => db.schema
  .table('scope', table => table.string('regex', 255).defaultTo(null));

module.exports.down = async db => db.schema
  .table('scope', table => table.dropColumn('regex'));

module.exports.configuration = { transaction: true };
