module.exports.up = async db => db.schema.table('user', (table) => {
  table.datetime('rfid_validity').after('rfid');

});

module.exports.down = async db => db.schema.table('user', (table) => {
  table.dropColumn('rfid_validity');
});

module.exports.configuration = { transaction: true };
