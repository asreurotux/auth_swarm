module.exports.up = async db => db.schema.createTable('profile_condition', (table) => {
  // conditions associated with profiles
  table.increments();
  table.integer('profile_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('profile');
  table.string('field_name', 4096).notNullable();
  table.string('field_op', 4096).notNullable();
  table.string('field_value', 4096).notNullable();
  table.datetime('created_at').notNullable();
  table.datetime('updated_at').notNullable();
})

  // Drop the existing profile id column in users table
  .table('user', table => table.dropForeign('profile_id', 'user_profile_id_foreign').dropColumn('profile_id'));

module.exports.down = async db => db.schema
  .table('user', table => table.integer('user_id')
    .integer('profile_id')
    .defaultTo(null)
    .unsigned()
    .references('id')
    .inTable('profile'))
  .dropTable('profile_condition');

module.exports.configuration = { transaction: true };
