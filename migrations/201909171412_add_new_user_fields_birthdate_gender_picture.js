module.exports.up = async db => db.schema.table('user', (table) => {
  table.datetime('birth_date').after('active');
  table.enu('gender', ['M', 'F']).after('birth_date').notNullable();
  table.string('picture', 2048).defaultTo(null).after('gender');
});

module.exports.down = async db => db.schema.table('user', (table) => {
  table.dropColumn('birth_date');
  table.dropColumn('gender');
  table.dropColumn('picture');
});

module.exports.configuration = { transaction: true };
