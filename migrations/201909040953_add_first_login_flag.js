module.exports.up = async db => db.schema.alterTable('user', (table) => {
  table.boolean('first_login').defaultTo(false).notNullable().after('pin');
});

module.exports.down = async db => db.schema.alterTable('user', (table) => {
  table.dropColumn('first_login');
});

module.exports.configuration = {
  transaction: true,
};