module.exports.up = async db => db.schema.createTable('user', (table) => {
  // users database table
  table.increments();
  table.text('rfid');
  table.string('name', 255).notNullable();
  table.string('email', 255).notNullable();
  table.string('phone', 120);
  table.string('user_name', 255).notNullable();
  table.text('password').notNullable();
  table.text('pin').notNullable();
  table.string('institute', 255);
  table.string('student_number', 255);
  table.boolean('external');
  table.boolean('active').notNullable();
  table.datetime('updated_at').notNullable();
  table.datetime('created_at').notNullable();
})

// user groups database table
  .createTable('user_group', (table) => {
    table.increments();
    table.string('name', 120).notNullable();
  })

// users to user groups many-to-many relationship database table
  .createTable('user_user_group', (table) => {
    table.increments();
    table.integer('user_id').unsigned().references('id').inTable('user');
    table.integer('user_group_id').unsigned().references('id').inTable('user_group');
    table.unique(['user_id', 'user_group_id']);
  })

// scopes database table
  .createTable('scope', (table) => {
    table.increments();
    table.string('name', 45);
    table.string('permission', 45);
    table.boolean('create');
    table.boolean('read');
    table.boolean('update');
    table.boolean('delete');
    table.boolean('approve');
    table.boolean('reject');
  })

// scopes to user groups many-to-many relationship database table
  .createTable('user_group_scope', (table) => {
    table.increments();
    table.integer('scope_id').unsigned().references('id').inTable('scope');
    table.integer('user_group_id').unsigned().references('id').inTable('user_group');
    table.unique(['scope_id', 'user_group_id']);
  })

// scopes to users many-to-many relationship database table
  .createTable('user_scope', (table) => {
    table.increments();
    table.integer('scope_id').unsigned().references('id').inTable('scope');
    table.integer('user_id').unsigned().references('id').inTable('user');
    table.unique(['scope_id', 'user_id']);
  })

// jwt tokens database table
  .createTable('jwt', (table) => {
    table.increments();
    table.text('token');
    table.boolean('active');
    table.datetime('created_at').notNullable();
    table.datetime('updated_at').notNullable();
  });

module.exports.down = async db => db.schema
  .dropTable('jwt')
  .dropTable('user_user_group')
  .dropTable('user_group_scope')
  .dropTable('user_group')
  .dropTable('user_scope')
  .dropTable('scope')
  .dropTable('user');

module.exports.configuration = { transaction: true };
