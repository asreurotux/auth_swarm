module.exports.up = async db => db.schema.createTable('user_device', (table) => {
  // user associated device ids
  table.increments();
  table.integer('user_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('user');
  table.string('device_id', 4096).notNullable();
})

  // user profiles table
  .createTable('profile', (table) => {
    table.increments();
    table.string('name', 255).notNullable();
    table.boolean('active').notNullable();
    table.datetime('created_at').notNullable();
    table.datetime('updated_at').notNullable();
  })

  // profile id column in users table
  .table('user', table => table
    .integer('profile_id')
    .defaultTo(null)
    .unsigned()
    .references('id')
    .inTable('profile'))

  // FB Messenger id column in users table
  .table('user', table => table.string('fb_messenger_id', 4096).defaultTo(null));

module.exports.down = async db => db.schema
  .table('user', table => table.dropColumn('profile_id'))
  .table('user', table => table.dropColumn('fb_messenger_id'))
  .dropTable('profile')
  .dropTable('user_device');

module.exports.configuration = { transaction: true };
