module.exports.up = async db => db.schema
  .dropTable('profile_condition')

  .createTable('target', (table) => {
    table.increments();
    table.string('name', 255).notNullable();
    table.boolean('active').notNullable();
    table.datetime('created_at').notNullable();
    table.datetime('updated_at').notNullable();
  })

  .createTable('target_condition', (table) => {
    table.increments();
    table.integer('target_id')
      .notNullable()
      .unsigned()
      .references('id')
      .inTable('target');
    table.string('field_name', 4096).notNullable();
    table.string('field_op', 4096).notNullable();
    table.string('field_value', 4096).notNullable();
    table.datetime('created_at').notNullable();
    table.datetime('updated_at').notNullable();
  })

  .table('user', (table) => {
    table.integer('profile_id').notNullable()
      .unsigned().defaultTo(4)
      .references('id')
      .inTable('profile');
  });

module.exports.down = async db => db.schema.createTable('profile_condition', (table) => {
  // conditions associated with profiles
  table.increments();
  table.integer('profile_id')
    .notNullable()
    .unsigned()
    .references('id')
    .inTable('profile');
  table.string('field_name', 4096).notNullable();
  table.string('field_op', 4096).notNullable();
  table.string('field_value', 4096).notNullable();
  table.datetime('created_at').notNullable();
  table.datetime('updated_at').notNullable();
})

  .dropTable('target_condition')
  .dropTable('target')
  .table('user', table => table.dropForeign('profile_id', '').dropColumn('profile_id'));

module.exports.configuration = { transaction: true };
