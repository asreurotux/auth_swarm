module.exports.up = async (db) =>
    db.schema.table('user', table => table.string('salt', 40).after('password').notNullable().defaultTo(''));

module.exports.down = async (db) =>
    db.schema.table('user', table => table.dropColumn('salt'));

module.exports.configuration = {
    transaction: true
};