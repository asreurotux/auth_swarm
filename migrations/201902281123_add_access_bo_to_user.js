module.exports.up = async db => db.schema
  .table('user', table => table.boolean('can_access_BO').after('external').notNullable().defaultTo(false));

module.exports.down = async db => db.schema
  .table('user', table => table.dropColumn('can_access_BO'));

module.exports.configuration = { transaction: true };
