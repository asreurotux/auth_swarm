module.exports.up = async db => db.schema.alterTable('user', (table) => {
  table.enu('gender', ['M', 'F', 'U']).alter();
});

module.exports.down = async db => db.schema.alterTable('user', (table) => {
  table.enu('gender', ['M', 'F']).alter();
});
