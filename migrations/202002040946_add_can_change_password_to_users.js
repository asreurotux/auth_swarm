module.exports.up = async db => db.schema.alterTable('user', (table) => {
  table.boolean('can_change_password').default(false).after('student_number');
});

module.exports.down = async db => db.schema.alterTable('user', (table) => {
  table.dropColumn('can_change_password');
});

module.exports.configuration = { transaction: true };
