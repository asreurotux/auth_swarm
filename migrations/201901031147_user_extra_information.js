module.exports.up = async db => db.schema
  .table('user', table => table.string('address', 255).defaultTo(null))
  .table('user', table => table.string('postal_code', 15).defaultTo(null))
  .table('user', table => table.string('city', 255).defaultTo(null))
  .table('user', table => table.string('country', 255).defaultTo(null))
  .table('user', table => table.string('tin', 11).defaultTo(null));

module.exports.down = async db => db.schema
  .table('user', table => table.dropColumn('address'))
  .table('user', table => table.dropColumn('postal_code'))
  .table('user', table => table.dropColumn('city'))
  .table('user', table => table.dropColumn('country'))
  .table('user', table => table.dropColumn('tin'));

module.exports.configuration = { transaction: true };
