FROM node:lts-stretch

ARG NODE_ENV

RUN mkdir /app

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm i -g moleculer

RUN if [ "$NODE_ENV" = "development" ]; \
	then npm install;  \
	else npm install --production; \
	fi

COPY . .

CMD if [ "$NODE_ENV" = "development" ]; \
	then npm run dev;  \
	else npm start; \
	fi
